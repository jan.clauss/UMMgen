<?php

		$portalaktionen=array(
			'hack' => 'HACK_PORTAL',
			'installMod' => 'INSTALL_MOD',
			'captureOrUpgrade' => 'CAPTURE_PORTAL',
			'createLink' => 'CREATE_LINK',
			'createField' => 'CREATE_FIELD',
			'enterPassphrase' => 'PASSPHRASE',
			'viewWaypoint' => 'HACK_PORTAL', // gibt es in UMM nicht
		);
		
		$lastpos['distance']=0.00005;
		$lastpos['lat']=200;
		$lastpos['lng']=200;



	class location
	{
		public $latitude=200;
		public $longitude=200;
	}
	
	class passprase
	{
		public $question='';
		public $_single_passphrase='';
	}
	
	class objective
	{
		public $type='';
		public $passphrase_params=null;
		
		function setData($portal) {
			global
				$portalaktionen;
			if(IsSet($portalaktionen[$portal['objective']])) {
				$this->type=$portalaktionen[$portal['objective']];
			}else{
				$this->type='HACK_PORTAL'; //Alle unbekannten Aktionen auf Hack setzen
			}
			$this->passphrase_params=new passprase(); // In den UMM-Json von mir ist das immer gesetzt, auch wenn leer. deshalb auch hier...
		}
	}
	
	class portal
	{
		public $description='';
		public $guid='';
		public $imageUrl='';
		public $isOrnamented=false;
		public $isStartPoint=false;
		public $location=null;
		public $title='';
		public $type='PORTAL';
		public $objective=null;
		
		function setData($portal) {
			global $lastpos;
			$this->guid=$portal['poi']['id'];
			if(IsSet($portal['poi']['title'])) {
				$this->title=$portal['poi']['title'];
			}else{
				$this->title=$portal['poi']['type']; // Kein Name vorhanden. Portal gelöscht? Typ als Namen, z.B. unavailable
			}
			$this->type=strtoupper($portal['poi']['type']);
			$this->location=new location();
			if(IsSet($portal['poi']['latitude'])) {
				$this->location->latitude=$portal['poi']['latitude'];
				$lastpos['lat']=$portal['poi']['latitude'];;
				$this->location->longitude=$portal['poi']['longitude'];
				$lastpos['lng']=$portal['poi']['longitude'];
			}else{
				$this->location->latitude=$lastpos['lat']+$lastpos['distance'];
				$lastpos['lat']=$this->location->latitude;
				$this->location->longitude=$lastpos['lng']+$lastpos['distance'];
				$lastpos['lng']=$this->location->longitude;
			}
			// Bannergress: PORTAL, FIELDTRIPWAYPOINT
			$this->objective=new objective();
			$this->objective->setData($portal);
		}
		
	}

	class mission implements JsonSerializable
	{
		public $missionTitle='';
		public $missionDescription='';
		public $missionPicture=''; // Original nicht enthalten
		public $portals=array();
		
		public function setData($missiondata, $description) 
		{
			$this->missionTitle=$missiondata['title'];
			$this->missionDescription=$description;
			$this->missionPicture=$missiondata['picture'];
			foreach($missiondata['steps'] AS $step) {
				$portal=new portal();
				$portal->setData($step);
				$this->portals[]=$portal;
			}
		}
		
		public function jsonSerialize()
		{
			return [
				'missionTitle' => $this->missionTitle,
				'missionDescription' => $this->missionDescription,
				'portals' => $this->portals,
//				'missionPicture'=> $this->missionPicture,
			];
		}
	}

	class mosaik
	{
		public $missionSetName='';
		public $missionSetDescription='';
		public $currentMission=0;
		public $plannedBannerLength=0;
		public $titleFormat='T NN-M';
		public $fileFormatVersion=2;
		public $missions=array();
		
		public function setData($bgdata)
		{
			if(!is_array($bgdata)) $bgdata=json_decode($bgdata, true);
			if(!is_array($bgdata)) return false;
			$this->missionSetName=$bgdata['title'];
			$this->missionSetDescription=$bgdata['description'];
			$this->currentMission=$bgdata['numberOfMissions'];
			$this->plannedBannerLength=$bgdata['numberOfMissions'];
//			$this->titleFormat='T NN-M'; // Keine Fehlenden Objekte also Standard
//			$this->fileFormatVersion=2; // Keine Fehlenden Objekte also Standard
			foreach($bgdata['missions'] AS $missiondata) {
				$missionobj=new mission();
				$missionobj->setData($missiondata, $this->missionSetDescription);
				$this->missions[]=$missionobj;
			}
		}
	}
	
?>