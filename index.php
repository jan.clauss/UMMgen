<?php
	$bannergress_uri='https://bannergress.com';
	$bannergress_api='https://api.bannergress.com';
	$bannergress_preg='#https://bannergress\.com/banner/([-a-z0-9%]+)#i';

	include 'objects.php.inc';
	
/***************************************
* Download des Mosaiks als ZIP-Datei
*  dazu wird die Bannergress json neu von Bannergress geladen
**/
if(IsSet($_POST['bglink'])) { // Download ausgewählt
	// Bannergress json laden
	if(!preg_match($bannergress_preg, $_POST['bglink'], $arr)) die("ungültiger Bannergress-Link");
	$bannergressjson=file_get_contents($bannergress_api.'/bnrs/'.$arr[1]);
	if($bannergressjson == false) die("Fehler beim Download des Mosaiks von Bannergress");
	// UMMObj erstellen
	$BGarr=json_decode($bannergressjson, true);
	$UMMobj=new mosaik();
	$UMMobj->setData($BGarr);
	// ZIP-Datei erstellen 
	$zip = new ZipArchive; 
	$zipname=sys_get_temp_dir().'/bg'.date('Y-m-d H:i:s').'.zip';
	$status = $zip->open($zipname, ZipArchive::CREATE); 
	if($status == false) die("Fehler beim Erstellen des ZIP-Archives");
	// Link in Datei speichern
	$zip->addFromString('track.lnk', $_POST['bglink']);
	// JSON in Datei speichern
	$ummjson=json_encode($UMMobj, JSON_PRETTY_PRINT);
	$zip->addFromString('track.json', $ummjson);
	// Bilder downloaden
	$imgname=sys_get_temp_dir().'/bgimg'.date('Y-m-d H:i:s').'.tmp';
	$errinfo='';
	$imgidx=1;
	foreach($UMMobj->missions AS $mission) {
		if(IsSet($mission->missionPicture) and $mission->missionPicture <> '') { // Ist ein Bild vorhanden?
			// Bild downloaden
			$img=fopen($imgname, 'w');
			if($img === false) die('Fehler beim erstellen der Temp-Datei');
			// Curl zum Bild laden
			$ch = curl_init($mission->missionPicture);
			curl_setopt($ch, CURLOPT_FILE, $img);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			curl_exec($ch);
			if(curl_errno($ch)) {
				$errinfo.="Curl Fehler beim Download von $imgidx:".$mission->missionPicture;
			}else{
				$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
				fclose($img);
				if($statusCode <> 200) die("Status Code: " . $statusCode);
				// Dateityp bestimmen 
				$mimetyp=mime_content_type($imgname);
				switch($mimetyp) {
					case 'image/jpg':
						$endung='jpg';
					break;
					case 'image/png':
						$endung='png';
					break;
					default:
						die ("Mimetyp: $mimetyp");
					break;
				}
				$digits=2;
				if(count($UMMobj->missions) > 99) $digits=3;
				if(count($UMMobj->missions) > 999) $digits=4;
				$zip->addFromString('image_'.str_pad($imgidx, $digits, '0', STR_PAD_LEFT).'.'.$endung, file_get_contents($imgname));
			}
			unlink($imgname);
		}else{
			$eerinfo.="Image $imgidx missing\n";
		}
		$imgidx++;
	}
	if($errinfo <> '') {
		$zip->addFromString('error.info', $errinfo); // Fehler gefunden, in ZIP einfügen
	}
	// ZIP schließen
	$zip->close();
	// ZIP senden
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=\"track.zip\"");
	readfile($zipname);
	
	exit;
	
}

/***************************************
* Formular zum erfassen des BG-Links
**/
?>
<!doctype html>
<html lang='de'>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>janc70 - UMMGen</title>
	</head>
	<body>
		Dieses Tool unterstützt Dich beim laden Deiner alten Missionen in das Ultimate Mission Maker-Format. Bitte beachte das geistige Eigentum, die Interessen anderer Ersteller, wenn Du Fremde Mosaike herunter lädst<br>
		This tool helps you to download your old missions into the Ultimate Mission Maker format. Please respect the intellectual property, the interests of other creators when downloading other people's mosaics<br>
		<br>
		Trage einen Bannergress Link in das Eingabefeld ein<br>
		Enter a Bannergress link in the input field<br>
		<form enctype="multipart/form-data" method='post'>
<?php
	$link='';
	if(IsSet($_POST['link'])) $link=$_POST['link'];
	if(IsSet($_POST['bglink'])) $link=$_POST['bglink'];
	echo "<input type='text' name='link' value='$link' style='width:90%;'><br>\n";
?>
			<input type='submit' name='send' value='Auswerten'>
		</form>
<?php

/***************************************
* Ausgabe von Bannergress json und UMM json als Textareas
* Bilder als Tabelle
**/

if(IsSet($_POST['link'])) {
	if(preg_match($bannergress_preg, $_POST['link'], $arr)) {
		echo $arr[1];
		if($bannergressjson=file_get_contents($bannergress_api.'/bnrs/'.$arr[1])) {
			$BGarr=json_decode($bannergressjson, true);
			
			// Ausgabe des Bannergress JSON
			echo "<br>Bannergress:<br><textarea name='bgjson' style='width:90%' rows=10>".print_r($BGarr, true)."</textarea>";

			// UMM Object
			$UMMobj=new mosaik();
			$UMMobj->setData($BGarr);
			
			// Ausgabe des UMM-JSON
			echo "<br>UMMjson:<br><textarea name='ummjson' style='width:90%' rows=10>".json_encode($UMMobj, JSON_PRETTY_PRINT)."</textarea>";
			// Form zum erstellen des ZIP-Files
			echo "<form method='post'>\n"; // Form für Dateispeicherung
			echo "<input type='hidden' name='bglink' value='$link'>\n";
			echo "<br><input type='submit' name='save' value='Speichern'>";
			echo "</form>\n"			;
			// Missionsbilder
			echo "<table>";
			$idx=0;
			foreach($UMMobj->missions AS $mission) {
				$idx++;
				$imgarr[$idx]=$mission->missionPicture;
			}
			$pos=0;
			for($i=$idx; $i > 0; $i--) {
				$pos++;
				if($pos==1) {
					echo "<tr>";
				}
				echo "<td><img src='".$imgarr[$i]."' style='width:90%;'></td>";
				if($pos==6) {
					echo "</tr>\n";
					$pos=0;
				}
			}
			echo "</table>\n";
		}
	}
}
?>
<br>
<br>
Kontakt: Jan Clauß, info at janc70 point rocks

